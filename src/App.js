import './App.css';
import {useEffect, useState} from "react";

const greetings = {
    spanish: "hola",
    french: "bonjour",
    german: "guten tag",
    italian: "salve",
    japanese: "konnichiwa",
    english: "greetings"
}

const App = () => {

    const [name, setName] = useState("");

    return (
        <div className="App">
            <header className="App-header">
                <div>
                    <label htmlFor={"name"}>Please enter your name </label>
                    <input value={name} type={"text"} name={"name"} id={"name"}/>
                </div>
                <div>
                    <Greeting name={name}/>
                </div>
            </header>
        </div>
    );
}

const Greeting = ({name}) => {

    const [greetingId, setGreetingId] = useState("english");
    const [greeting, setGreeting] = useState("");
    const [loading, setLoading] = useState(false);

    return (
        <>
            <h1>{`${greeting} ${name}`}</h1>
            <div>
                <label htmlFor={"name"}>Please select a greeting </label>
                <select value={greetingId} onChange={(event => setGreetingId(event.target.value))}>
                    {Object.keys(greetings).map(id => {
                        return (
                            <option key={id} value={id}>{id}</option>
                        )
                    })}
                </select>
            </div>
        </>
    )
}

const fetchGreetingById = async id => {
    const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
    await sleep(1000)
    return greetings[id];
}


export default App;
