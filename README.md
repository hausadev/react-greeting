# setup
Based on create-react-app, just a little to the left.

you will need node/npm installed I used v14.19.1 and 6.14.16

To set up, head over to the root of the project and run `npm install` in your terminal, console, or IDE of choice.

`npm` `start` script is set up to run `webpack-dev-server` in watch mode so changes are picked up automatically

to run `npm run start` in you terminal, console, or IDE of choice.

`src/App.js` should be the only file need to complete this exercise.

Good Luck!



1. In the `<App/>` functional component
   * add an `onChange` event handler to the name input that
      * captures the event
      * get the entered value
      * uses the `name` state setter to update the `name` state variable
   * adjust the `<div/>` containing the `<Greeting/>` component so that it only renders when
      * `name` state variable is not `null`
      * `name` state variable is not an empty string
2. In the `<Greeting/>` component
   * modify the `<h1/>` so that it
      * displays "_loading_" when the state variable loading is `true`
      * displays the values of the state variables `greeting` and `name` when loading is `false`
   * add a `useEffect()` hook that
      * fire only when the state of `greetingId` has changed
      * uses the `greetingId` to asynchronously call and wait for `fetchGreetingById(greetingId)`
      * set the greeting returned by `fetchGreetingById` to the `greeting` state variable using its setter
      * set loading to `true` while waiting for the `fetchGreetingById` promise to complete
      * set loading to `false` once `fetchGreetingById` has resolved and `greeting` state variable has been set